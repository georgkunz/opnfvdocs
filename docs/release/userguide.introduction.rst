.. _opnfv-user-config:

.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. SPDX-License-Identifier: CC-BY-4.0
.. (c) Anuket CCC, AT&T, and other contributors

================================
User Guide & Configuration Guide
================================

Abstract
========

One of Anuket's project goals is to reduce time to integrate and deploy NFV infrastructure and onboard VNF/CNFs
for those who supply components and those who operationalize these platforms. The Anuket community
does this by implementing, testing and deploying tools for conformance and performance of NFV infrastructure, aligned
with industry reference architectures. This document provides guidance and instructions for using platform
features designed to support the tools that are made available in the Anuket
Kali release.

This document is not intended to replace or replicate documentation from other
upstream open source projects such as KVM, OpenDaylight, OpenStack, etc., but to highlight the
features and capabilities delivered through the Anuket project.


Introduction
============

Anuket provides a infrastructure deployment options, which
are able to be installed to host virtualised network functions (VNFs) and cloud network functions (CNFs).
This document intends to help users leverage the features and
capabilities delivered by Anuket.

Feature Overview
================

The following links outline the feature deliverables from participating Anuket
projects in the Kali release. Each of the participating projects provides
detailed descriptions about the features delivered including use cases,
implementation, and configuration specifics in the project documentation.

The following Configuration Guides and User Guides assume that the reader already has some
knowledge about a given project's specifics and capabilities. These Guides
are intended to allow users to deploy and implement features that are part of the 
Anuket Kali release.

If you are unsure about the specifics of a given project, please refer to the
Anuket wiki page at http://wiki.anuket.io for more details.


Feature Configuration Guides
============================

* :ref:`AIRSHIP Configuration Guide <airship-installation>`
* :ref:`Barometer Configuration Guide <barometer-configguide>`
* :ref:`CIRV-SDV Configuration Guide <cirv-sdv-configguide>`
* :ref:`Kuberef Configuration Guide <kuberef-configguide>`
* :doc:`ViNePERF Configuration Guide <vineperf:testing/user/configguide/index>`
* :doc:`Functest Installation Guide <functest:testing/user/configguide/index>`




Feature User Guides
===================

* :ref:`Barometer User Guide <barometer-userguide>`
* :ref:`CIRV-SDV User Guide <cirv-sdv-userguide>`
* :ref:`Kuberef User Guide <kuberef-userguide>`
* :doc:`ViNePERF User Guide <vineperf:testing/user/configguide/index>`
* :doc:`Functest User Guide <functest:testing/user/userguide/index>`

