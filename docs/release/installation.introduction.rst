.. _opnfv-installation:

.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. SPDX-License-Identifier: CC-BY-4.0
.. (c) Anuket CCC, AT&T, and other contributors

============
Installation
============

Abstract
========

This is the installation document for the Anuket Kali release, please use this document for further reference. 

Installation Procedure
======================

Each of the test Frameworks and other projects will have separate installations procedures: see the individual project documentation.

- :ref:`Airship Installation Guide <airship-installation>`
- :ref:`Barometer Installation Guide <barometer-docker-userguide>`
- :ref:`CIRV-SDV Installation Guide <cirv-sdv-installation>`
- :ref:`Kuberef Installation Guide <kuberef-installation>`
- :ref:`ViNePERF installation Guide <vineperf-installation>`

*   :doc:`Functest Installation Guide <functest:testing/user/configguide/index>`


Anuket Test Frameworks
======================

If you have elected to install using the Anuket Kali Release toolchain,
you can begin to test once the installation is completed.
The basic deployment validation only addresses some of the capabilities in
any platform and you may want to execute more exhaustive tests. Some investigation will be required to
select the right test suites to run on your platform.

Many of the Anuket test projects provide user-guide documentation and installation instructions in :ref:`this document <testing-userguide>`

Anuket Reference Specifications
===============================

There is no need to "install" the Anuket Reference Specifications! You can view them here:

.. *    :doc:`Anuket Reference Specifications <cntt-cntt:/index>`

*    `Anuket Reference Specifications <https://cntt.readthedocs.io/en/stable-kali/index.html>`_
