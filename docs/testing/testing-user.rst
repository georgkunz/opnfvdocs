.. _testing-userguide:

.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. http://creativecommons.org/licenses/by/4.0

===================
Testing User Guides
===================

This page provides the links to the installation, configuration and user guides
of the different test projects.

OPNFV Verified Program --> Anuket Assured Program
-------------------------------------------------
.. toctree::
   :maxdepth: 1

* OVP releases were on a separate schedule from  OPNFV.  You may find
  the current status and latest documentation for OVP at
  `<http://verified.opnfv.org>`_. The next incarnation of OVP will be the 
  Anuket Assured Program.

Functest
--------
.. toctree::
   :maxdepth: 1

*   :doc:`Functest Installation Guide <functest:testing/user/configguide/index>`
*   :doc:`Functest User Guide <functest:testing/user/userguide/index>`

SampleVNF
---------
.. toctree::
   :maxdepth: 1

*   :doc:`SampleVNF User Guide <samplevnf:testing/user/userguide/index>`

VSPERF
------
.. toctree::
   :maxdepth: 1

*   :doc:`ViNePERF Configuration and User Guide <vineperf:testing/user/configguide/index>`
*   :doc:`ViNePERF Test Guide <vineperf:testing/user/userguide/index>`


