.. _testing-dev:

.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. http://creativecommons.org/licenses/by/4.0

========================
Testing Developer Guides
========================

Testing group
-------------
.. toctree::
   :maxdepth: 1

   ./developer/devguide/index

OPNFV Verified Program –> Anuket Assured Program
------------------------------------------------

* OVP releases were on a separate schedule from  OPNFV.  You may find
  the current status and latest documentation for OVP at
  `<http://verified.opnfv.org>`_. The next incarnation of OVP will be the 
  Anuket Assured Program.

Functest
--------

* :doc:`Functest Developer Guide <functest:testing/developer/devguide/index>`

ViNePERF
--------

* :doc:`ViNePERF Developer Guide <vineperf:testing/developer/devguide/index>`
