.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. SPDX-License-Identifier: CC-BY-4.0

Anuket Documentation
====================

The Anuket project facilitates the development and evolution
of cloud components across various open source ecosystems. 
Through preparation of reference specifications,
system level integration, testing and deployment, Anuket creates 
reference implementations and conformance tests to accelerate the 
transformation of enterprise
and service provider networks. Participation is open to anyone,
whether you are an employee of a member company or just passionate
about network transformation.

`Anuket Reference Specifications <https://cntt.readthedocs.io/en/stable-kali/index.html>`_

Installation Guides and More
----------------------------

.. toctree::
   :maxdepth: 1

   release/installation.introduction
   release/userguide.introduction
   release/release-notes


Testing Frameworks
------------------

.. toctree::
   :maxdepth: 1

   testing/testing-user
   testing/testing-dev

Infrastructure
--------------

.. toctree::
   :maxdepth: 1

   infrastructure/overview
   infrastructure/ci
..   infrastructure/xci

Developer Guide
---------------

.. toctree::
   :maxdepth: 1

   how-to-use-docs/index

Found a typo or any other feedback? Send us an email_ or
talk to us on Slack_.

.. _email: mailto:users@opnfv.org
.. _Slack: https://anuketworkspace.slack.com/
